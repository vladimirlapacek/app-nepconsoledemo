﻿using Common.Logging;
using Ncr.Nep.Client;
using Ncr.Nep.Client.Security;
using Ncr.Nep.Storage;
using System.Threading;
using System.Threading.Tasks;

namespace DemoApp3
{
    class Program
    {
        private static ILog Log = LogManager.GetLogger<Program>();
        private static IStorageContainers containerService;

        public static void Main(string[] args)
        {
            NepApplication.Init(app =>
            {
                containerService = app.ServiceFactory.Create<IStorageContainers>();
            });

            CreateContainer().GetAwaiter().GetResult();
            DoSomething().GetAwaiter().GetResult();
            Thread.Sleep(30 * 1000);
        }

        private static async Task DoSomething()
        {
            await TechnicalUserScope.Execute("storeadmin", async () =>
            {
                var result = await containerService.FindByCriteriaAsync(0, 100, "*");

                Log.InfoFormat("Found {0} containers:", result.PageContent.Count);
                foreach (var container in result.PageContent)
                {
                    Log.Info(container.ContainerId.ContainerName);
                }
            });
        }

        private static async Task CreateContainer()
        {
            await TechnicalUserScope.Execute("storeadmin", async () => {
                await containerService.CreateAsync(GenerateCreateContainerRequest(), null);
            });
        }

        private static CreateContainerRequest GenerateCreateContainerRequest()
        {

            var cid = new ContainerIdData();
            cid.ContainerName = "TestContainer2";

            //var ownid = new OrganizationUserIdData();
            //ownid.OrganizationName = "ncr-console";
            //ownid.Username = "storeadmin";

            var expPolicy = new ExpirationPolicyData();
            expPolicy.Hours = 24;

            var ccr = new CreateContainerRequest()
            {
                ContainerId = cid,
                ExpirationPolicy = expPolicy,
                ReadPolicy = CreateContainerRequestReadPolicy.AUTHENTICATED_USERS,
                WritePolicy = CreateContainerRequestWritePolicy.AUTHENTICATED_USERS
            };

            return ccr;
        }
    }
}
